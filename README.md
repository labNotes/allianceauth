Alliance Auth
============

[![Chat on Discord](https://img.shields.io/discord/399006117012832262.svg)](https://discord.gg/fjnHAmk)
[![Documentation Status](https://readthedocs.org/projects/allianceauth/badge/?version=latest)](http://allianceauth.readthedocs.io/?badge=latest)
[![pipeline status](https://gitlab.com/allianceauth/allianceauth/badges/master/pipeline.svg)](https://gitlab.com/allianceauth/allianceauth/commits/master)
[![coverage report](https://gitlab.com/allianceauth/allianceauth/badges/master/coverage.svg)](https://gitlab.com/allianceauth/allianceauth/commits/master)



An auth system for EVE Online to help in-game organizations manage online service access.

[Read the docs here.](http://allianceauth.rtfd.io)

[Get help on Discord](https://discord.gg/fjnHAmk) or submit an Issue.


Active Developers:

 - [Adarnof](https://gitlab.com/adarnof/)
 - [Basraah](https://gitlab.com/basraah/)
 - [Ariel Rin](https://gitlab.com/soratidus999/)
 - [Col Crunch](https://gitlab.com/colcrunch/)

Beta Testers / Bug Fixers:

 - [ghoti](https://gitlab.com/ChainsawMcGinny/)
 - [mmolitor87](https://gitlab.com/mmolitor87/)
 - [TargetZ3R0](https://github.com/TargetZ3R0)
 - [kaezon](https://github.com/kaezon/)
 - [orbitroom](https://github.com/orbitroom/)
 - [tehfiend](https://github.com/tehfiend/)

Special thanks to [Nikdoof](https://github.com/nikdoof/), as his [auth](https://github.com/nikdoof/test-auth) was the foundation for the original work on this project.

### Contributing
Make sure you have signed the [License Agreement](https://developers.eveonline.com/resource/license-agreement) by logging in at [https://developers.eveonline.com](https://developers.eveonline.com) before submitting any pull requests.
